# import class modules
import circle_class as cc
import triangle_class as tc

# instantiate a circle and a triangle object
circ = cc.Circle()
tri = tc.Triangle()

# initialize attributes
tri.height = 1
tri.width = 4
circ.radius = 2

# compute / print areas for the two objects
print 'Area of circle is:', circ.getArea()
print 'Area of triangle is:', tri.getArea()
